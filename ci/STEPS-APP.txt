
gcloud beta container --project "ro-mpl-cust-csvm-cl-dev-becust" clusters create "pilot-cluster-dev1" --zone "us-central1-c" --no-enable-basic-auth --cluster-version "1.22.12-gke.2300" --release-channel "regular" --machine-type "e2-medium" --image-type "COS_CONTAINERD" --disk-type "pd-standard" --disk-size "100" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node "110" --num-nodes "3" --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/ro-mpl-cust-csvm-cl-dev-becust/global/networks/default" --subnetwork "projects/ro-mpl-cust-csvm-cl-dev-becust/regions/us-central1/subnetworks/default" --no-enable-intra-node-visibility --default-max-pods-per-node "110" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --node-locations "us-central1-c" 

gcloud beta container --project "ro-mpl-cust-csvm-cl-dev-becust" clusters create "pilot-cluster-dev1" --zone "us-central1-c" --no-enable-basic-auth --cluster-version "1.22.12-gke.2300" --release-channel "regular" --machine-type "e2-medium" --image-type "UBUNTU_CONTAINERD" --disk-type "pd-standard" --disk-size "100" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node "110" --num-nodes "3" --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/ro-mpl-cust-csvm-cl-dev-becust/global/networks/default" --subnetwork "projects/ro-mpl-cust-csvm-cl-dev-becust/regions/us-central1/subnetworks/default" --no-enable-intra-node-visibility --default-max-pods-per-node "110" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --node-locations "us-central1-c"

gcloud container clusters get-credentials pilot-cluster-dev1 --region us-central1 --project ro-mpl-cust-csvm-cl-dev-becust

gcloud auth configure-docker


EJECUTAR 
1.1 make  step-credentials-regional CLUSTER_NAME=pilot-cluster-dev1 PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust

1.2 make step-builder PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust APP_VERSION=1.0.0

2.1. make step-push-app PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust APP_VERSION=1.0.0

2.2  make step-publish PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust APP_VERSION=1.0.0 SERVICE_NAME=ms-service-app CLUSTER_NAME=pilot-cluster-dev1

2.3  make step-deploy-run-app PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust APP_VERSION=1.0.2 SERVICE_NAME=ms-service-app








FOR MANUAL TRIGGER (2.2):
 FOR CLOUD RUN 
  gcloud builds submit --config cloudbuild.yaml --project ro-mpl-cust-csvm-cl-dev-becust  --substitutions=_PROYECT_NAME="ro-mpl-cust-csvm-cl-dev-becust",_APP_VERSION="1.0.0",_SERVICE_NAME="ms-service-app",_CLUSTER_ZONE="us-central1-f",_CLUSTER_NAME="autopilot-cluster-dev1"
 
 FOR K8S 
  gcloud builds submit --config cloudbuild.yaml --project ro-mpl-cust-csvm-cl-dev-becust  --substitutions="_PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust,_APP_VERSION=1.0.0,_SERVICE_NAME=ms-service-app,_CLUSTER_ZONE=us-central1,_CLUSTER_NAME=autopilot-cluster-dev1"
  gcloud builds submit --config cloudbuild.yaml --project ro-mpl-cust-csvm-cl-dev-becust  --substitutions=_PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust,_APP_VERSION=1.0.0,_SERVICE_NAME=ms-service-app,_CLUSTER_ZONE=us-central1,_CLUSTER_NAME=pilot-cluster-dev1
  
 
 run local
 
make docker-build ROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust APP_VERSION=1.0.0
 
make docker-run-all-local PROYECT_NAME=ro-mpl-cust-csvm-cl-dev-becust APP_VERSION=1.0.0
 
     # Deploy container image to Cloud Run
 #   - name: 'gcr.io/cloud-builders/gcloud'
 #     args: [ 'run',
 #             'deploy',
 #             '${_SERVICE_NAME}',
 #             '--image', 'gcr.io/${_PROYECT_NAME}/ms-service-app:${_APP_VERSION}',
 #             '--region', 'us-central1',
 #             '--platform', 'managed',
 #             '--allow-unauthenticated',
 #             '--set-env-vars',
 #             'SPRING_PROFILE=dev,spring.profiles.active=dev,SPRING_PROFILES_ACTIVE=dev']
 
 
 