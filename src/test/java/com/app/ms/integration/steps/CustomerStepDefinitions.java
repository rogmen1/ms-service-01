package com.app.ms.integration.steps;


import com.app.ms.integration.ResponseResults;
import com.app.ms.integration.ServiceInterpreter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomerStepDefinitions {

    private final Logger log = LoggerFactory.getLogger(CustomerStepDefinitions.class);

    @Autowired
    private ServiceInterpreter httpClient;

    ResponseResults response;

    @Given("se consulta por el recurso {string}")
    public void se_consulta_por_el_recurso(String recurso) throws Throwable {
        assertEquals("health", recurso);
    }


    @When("el servcio estable es {string}")
    public void el_servcio_estable_es(String estado) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        response= httpClient.executeGet("/actuator/health");
        JsonNode jsonNode = objectMapper.readTree(response.getBody());
        assertEquals("UP", jsonNode.findValue("status").asText());


    }

    @Then("el estado debe ser {int}")
    public void el_estado_debe_ser(int codigo) throws Exception {
        assertEquals(200, response.getTheResponse().getStatusCode().value());
    }

    @Then("el estado no debe ser {int}")
    public void el_estado_no_debe_ser(int codigo) throws Exception {
        assertTrue(response.getTheResponse().getStatusCode().value() != codigo );
    }

}
