package com.app.ms.integration;

import com.app.ms.integration.CucumberSpringConfiguration;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/health",
        plugin = {"pretty", "html:target/cucumber/health"} )
public class HealthIntegrationTest {
}
