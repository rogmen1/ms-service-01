package com.app.ms.integration;

import com.app.ms.MsService01Application;
import com.app.ms.infra.config.AppConfig;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@CucumberContextConfiguration
@ContextConfiguration(classes = {MsService01Application.class , AppConfig.class}  )
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CucumberSpringConfiguration {
}
