package com.app.ms.infra.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.app.ms.domain.customer.entity.Customer;

@RunWith(MockitoJUnitRunner.class)
public class FiltersUtilsTest {

	@Test
	public void test() {
		Customer cust1 = Customer.builder().foid(1L).userInfo("01").build();
		Customer cust2 = Customer.builder().foid(2L).userInfo("01").build();
		Customer cust3 = Customer.builder().foid(3L).userInfo("03").build();
		Collection<Customer> list = Arrays.asList(cust1, cust2, cust3);

		List<Customer> distinctElements = list.stream().filter(FiltersUtils.distinctByKey(p -> p.getUserInfo()))
				.collect(Collectors.toList());

		System.out.println(distinctElements);

	}

}
