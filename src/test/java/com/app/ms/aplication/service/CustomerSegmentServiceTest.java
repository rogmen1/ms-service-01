package com.app.ms.aplication.service;

import com.app.ms.aplication.criteria.CustomerCriteria;
import com.app.ms.aplication.criteria.SegmentFilter;
import com.app.ms.aplication.model.CustomerDetail;
import com.app.ms.domain.customer.entity.Customer;
import com.app.ms.domain.customer.entity.Person;
import com.app.ms.domain.customer.entity.SegmentType;
import com.app.ms.domain.customer.service.CustomerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CustomerSegmentServiceTest {


    @Mock
    private CustomerService customerService ;

    @InjectMocks
    private CustomerSegmentService customerSegmentService ;

    private Set<CustomerDetail> customerDetailSet;

    @Before
    public void initTesting() throws  Exception{
        customerDetailSet= new HashSet<>();
        customerDetailSet.add(CustomerDetail.builder().customer(Customer.builder().foid(1L).build()).build());
        customerDetailSet.add(CustomerDetail.builder().customer(Customer.builder().foid(2L).build()).build());
    }

    @Test
    public void shouldGetAllCustomerForSegment() throws  Exception{
        CustomerCriteria criteria = CustomerCriteria.builder().segmentType(SegmentFilter.LOCAL).build();
        List<Customer> cus = new ArrayList<>();
        cus.add(Customer.builder()
                        .foid(1L)
                        .segmentType(new SegmentType())
                        .person(Person.builder().build())
                .build());
        when(customerService.findCustomerBySegment(any(SegmentFilter.class))).thenReturn(cus);
        List<CustomerDetail> list = customerSegmentService.displayInfoByCriteria(criteria);
        Assert.assertTrue(list.size()>0);
    }

    @Test
    public void shouldGetAllCustomer() throws  Exception{
        when(customerService.displayAllelements()).thenReturn(customerDetailSet);
        Set<CustomerDetail> list = customerSegmentService.displayAllelements();
        Assert.assertTrue(list.size()>0);

    }
}
