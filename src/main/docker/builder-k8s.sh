#!/bin/bash

set -xeuo pipefail

service_name=ms-service-app

tag_service=${APP_VERSION}

image_name="gcr.io/${PROYECT_NAME}/${service_name}:${tag_service}"

docker build -t $image_name -f src/main/docker/Dockerfile  .

docker push   $image_name 