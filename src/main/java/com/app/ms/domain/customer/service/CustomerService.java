package com.app.ms.domain.customer.service;

import com.app.ms.aplication.criteria.SegmentFilter;
import com.app.ms.aplication.model.CustomerDetail;
import com.app.ms.domain.customer.entity.Customer;
import com.app.ms.domain.customer.repository.CustomerRepository;
import com.app.ms.domain.customer.repository.PersonRepository;
import com.app.ms.infra.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CustomerService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private CustomerRepository customerRepository;

    public Customer findCustomer(Long idCustomer) throws NotFoundException {

        Optional<Customer> customer = customerRepository.findById(idCustomer);

        if (!customer.isPresent())
            throw new NotFoundException();

        return customer.orElse(Customer.builder().build());

    }

    public List<Customer> findCustomerBySegment(SegmentFilter segment)  throws NotFoundException {

        List<Customer> customers = customerRepository.findCustomerBySegmentType(segment.getValue());
        if (customers.isEmpty())
            throw new NotFoundException();

        return customers;


    }

    public Set<CustomerDetail> displayAllelements(){
        Set<CustomerDetail> customers = new HashSet<>();
        return customers;
    }
}
