package com.app.ms.domain.customer.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "Person")
public class Person implements Serializable {

    private static final long serialVersionUID = -6404997761311963205L;
    @Id
    @Column(name = "id_person", nullable = false)
    private Long idPerson;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "birth", nullable = false , columnDefinition = "DATETIME")
    private Date birth;
}
