package com.app.ms.domain.customer.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "segment_type")
public class SegmentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4734145292555145577L;
	@Id
	@Column(name = "code_segment", nullable = false)
	private int code;
	
	@Column(name = "description", nullable = false)
	private String description;
	
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

}
