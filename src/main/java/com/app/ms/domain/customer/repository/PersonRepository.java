package com.app.ms.domain.customer.repository;

import java.util.List;

import com.app.ms.domain.customer.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

	List<Person> findByFirstName(String firstName);
}
