package com.app.ms.domain.customer.repository;

import java.util.List;

import com.app.ms.domain.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	@Query(value ="SELECT * " + 
			"FROM customer c " + 
			"        INNER JOIN person p ON c.id_person_customer = p.id_person " + 
			"        INNER JOIN segment_type st ON c.code_segment_customer = st.code_segment " + 
			"WHERE st.code_segment = :code", nativeQuery = true)
	List<Customer> findCustomerBySegmentType(int code);
}
