package com.app.ms.presentation.adapter.rest;


import com.app.ms.aplication.criteria.CustomerCriteria;
import com.app.ms.aplication.criteria.SegmentFilter;
import com.app.ms.aplication.model.CustomerDetail;
import com.app.ms.aplication.service.ApplicationService;
import com.app.ms.common.infraestructure.monitoring.interceptor.MethodToLog;
import com.app.ms.infra.exceptions.NotFoundException;
import com.app.ms.presentation.port.request.CustomerRequest;
import com.app.ms.presentation.port.response.PayloadBase;
import com.app.ms.presentation.port.response.UserDetailResponse;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.RandomUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static com.app.ms.common.infraestructure.monitoring.SmartLogger.info;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/customers")
public class RestUserController {

    @Autowired
    private  ApplicationService<CustomerDetail, CustomerCriteria> customerService;

    Logger logger = LogManager.getLogger(RestUserController.class);


    @RequestMapping(path = "/customer-detail/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public UserDetailResponse getCustomerName(@PathVariable("id") Long idCustomer) throws NotFoundException {

        logger.info("Se empezara a buscar la informacion del Costumer Nro :" + idCustomer);
        CustomerDetail cus = customerService.getDetailInfo(CustomerCriteria.builder().idCustomer(idCustomer).build());
        return UserDetailResponse.builder().customerDetail(cus).build();

    }

    @MethodToLog("customerByLocation")
    @RequestMapping(path = "/customers/{segment}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE  )
    public List<CustomerDetail> customerByLocation(HttpServletRequest request, @PathVariable("segment") Integer segmentId) throws NotFoundException, InterruptedException {
    	 Thread.sleep(RandomUtils.nextLong(100, 900));
    	        SegmentFilter segmentType = SegmentFilter.getById(segmentId);
        List<CustomerDetail> list = customerService
                .displayInfoByCriteria(CustomerCriteria.builder().segmentType(segmentType).build());

        info().withAction("customerByLocation")
             .with("segment", segmentId)
             .to(log);
        return list;

    }

    @PutMapping(path = "/user-segment", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PayloadBase> updateSegmentCustomer(@RequestBody final CustomerRequest user) throws InterruptedException {

    	 Thread.sleep(RandomUtils.nextLong(100, 2000));
        CustomerDetail element =  customerService.updateElement(user.getCriteria());
        return new ResponseEntity<PayloadBase>(new PayloadBase(200, "update success..."), HttpStatus.OK);

    }
}
