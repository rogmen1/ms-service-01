package com.app.ms.presentation.port.response;

public class PayloadBase {

	
	private int code;
	private String nativeMessage;
	
	public PayloadBase() {
		// TODO Auto-generated constructor stub
	}

	public PayloadBase(int code, String nativeMessage) {
		super();
		this.code = code;
		this.nativeMessage = nativeMessage;
	}
	
	
	public int getCode() {
		return code;
	}
	
	public String getNativeMessage() {
		return nativeMessage;
	}
}
