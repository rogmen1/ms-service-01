package com.app.ms.presentation.port.request;

import com.app.ms.aplication.criteria.CustomerCriteria;
import lombok.Data;

@Data
public class CustomerRequest {

	private CustomerCriteria criteria;

}
