package com.app.ms.infra.config;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.OpenTelemetry;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableWebMvc
@EnableTransactionManagement
@EnableJpaAuditing
public class AppConfig {


	
	  @Bean
	  public OpenTelemetry openTelemetry() {
	    return GlobalOpenTelemetry.get();
	  }
	  
}
