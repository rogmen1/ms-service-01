package com.app.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsService01Application {

	public static void main(String[] args) {
		SpringApplication.run(MsService01Application.class, args);
	}

}
