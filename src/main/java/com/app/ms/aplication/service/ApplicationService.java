package com.app.ms.aplication.service;

import com.app.ms.infra.exceptions.NotFoundException;

import java.util.List;
import java.util.Set;

public interface ApplicationService<R, T> {

    R getDetailInfo(T t) throws NotFoundException;

    List<R> displayInfoByCriteria(T t) throws NotFoundException;

    Set<R> displayAllelements() throws  NotFoundException;
    
    R updateElement(T t) ;
}
