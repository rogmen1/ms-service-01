package com.app.ms.aplication.service;

import com.app.ms.aplication.criteria.CustomerCriteria;
import com.app.ms.aplication.model.CustomerDetail;
import com.app.ms.domain.customer.entity.Customer;
import com.app.ms.domain.customer.service.CustomerService;
import com.app.ms.infra.exceptions.NotFoundException;
import com.app.ms.presentation.adapter.rest.RestUserController;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import static com.app.ms.common.infraestructure.monitoring.SmartLogger.info;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service("CustomerSegmentService")
@Scope(  value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CustomerSegmentService implements ApplicationService<CustomerDetail, CustomerCriteria> {

    @Autowired
    private  CustomerService customerService;


    @Override
    public CustomerDetail getDetailInfo(CustomerCriteria criteria) throws NotFoundException {
        Customer cus = customerService.findCustomer(criteria.getIdCustomer());
        return CustomerDetail.builder().customer(cus).build();
    }

    @Override
    public List<CustomerDetail> displayInfoByCriteria(CustomerCriteria criteria) throws NotFoundException {
        List<Customer> customers = customerService.findCustomerBySegment(criteria.getSegmentType());
        List<CustomerDetail>  result = new ArrayList<>();
        customers.forEach( item ->{
            result.add(CustomerDetail.builder().customer(item).build());
        });
        info().withAction("displayInfoByCriteria")
        .with("getSegmentType", criteria.getSegmentType())
        .to(log);
        return result;
    }

    @Override
    public Set<CustomerDetail> displayAllelements() throws NotFoundException {
    /*
        Set<CustomerDetail> customer = new HashSet<>();
        List<Customer>  list = customerRepository.findAll();

        list.forEach(i -> {
            info().withAction("displayAll")
                    .with("foidCustomer", i.getFoid())
                    .with("UserInfo", i.getUserInfo())
                    .with("UserLocation", i.getUserLocation())

                    .to(log);
            customer.add(Customer
                    .builder().foid(i.getFoid()).userLocation(i.getUserInfo()).person(Person.builder()
                            .firstName(i.getPerson().getFirstName()).lastName(i.getPerson().getLastName()).build())
                    .build());
        });
        return customer;*/

        return customerService.displayAllelements();
    }

    @Override
    public CustomerDetail updateElement(CustomerCriteria criteria) {


        return CustomerDetail
                    .builder()
                    .customer(Customer.builder()
                                      .foid(criteria.getFoid())
                                      .build())
                    .build();
    }
}
