package com.app.ms.aplication.model;

import com.app.ms.domain.customer.entity.Customer;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Setter
@Getter
@Builder
public class CustomerDetail implements Serializable {


    private Customer customer;

}
