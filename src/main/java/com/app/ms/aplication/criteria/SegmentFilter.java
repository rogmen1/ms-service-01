package com.app.ms.aplication.criteria;

import java.util.Arrays;

public enum SegmentFilter {

	LOCAL(1),
	ZONAL(2),
	GLOBAL(4),
	REGIONAL(3);
	
	private int value;
	
	private SegmentFilter(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}

	public static SegmentFilter getById(int id){

		return Arrays.stream(values())
					 .filter(e -> e.getValue() == id)
				     .findFirst().get();

	}
}
