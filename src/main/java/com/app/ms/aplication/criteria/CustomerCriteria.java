package com.app.ms.aplication.criteria;



import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerCriteria {

	private Long idCustomer;
	private SegmentFilter segmentType;
	private Long foid;
	private String userLocation;
	private String userInfo;
	private Long idPerson;

}
