INSERT INTO person(id_person,first_name,last_name,birth)VALUES(1000, 'Roger', 'Mendez', '2020-06-15 00:00:00');
INSERT INTO person(id_person,first_name,last_name,birth)VALUES(1001, 'david', 'Mendez', '2020-06-16 00:00:00');
INSERT INTO person(id_person,first_name,last_name,birth)VALUES(1002, 'Peter', 'Arrays', '2020-07-10 00:00:00');
INSERT INTO person(id_person,first_name,last_name,birth)VALUES(1003, 'Bruce', 'Waine', '2020-09-14 00:00:00');
INSERT INTO person(id_person,first_name,last_name,birth)VALUES(1004, 'Roger', 'Flowers', '2018-07-10 00:00:00');
INSERT INTO person(id_person,first_name,last_name,birth)VALUES(1005, 'Willi', 'Waine', '1999-05-14 14:00:00');

INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000000,1000,1,'Simple charge','Av. First');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000001,1001,3,'Simple pay','Av. WallStreet');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000002,1002,2,'Double charge','Second Wall');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000003,1003,2,'Simple charge','Downtown');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000004,1002,3,'Simple Applier charge','Second Wall');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000005,1004,3,'Double Applier charge','Downtown');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000006,1005,3,'Complex Applier charge','Downtown');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000007,1001,4,'Simple Applier charge','Second Wall');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000008,1002,4,'Double Applier charge','Downtown');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000009,1003,4,'Complex Applier charge','Downtown');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000010,1004,4,'Complex Applier charge','Downtown');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000011,1005,4,'Complex Applier charge','Downtown');
INSERT INTO customer(foid,id_person_customer,code_segment_customer,user_info,user_location)VALUES(1000012,1001,1,'Simple charge','Av. First 1000');

INSERT INTO segment_type(code_segment,description,enabled)VALUES(1,'Local',1);
INSERT INTO segment_type(code_segment,description,enabled)VALUES(2,'Zonal',1);
INSERT INTO segment_type(code_segment,description,enabled)VALUES(3,'Regional',1);
INSERT INTO segment_type(code_segment,description,enabled)VALUES(4,'Global',1);

