CREATE TABLE segment_type (
  code_segment int(11) NOT NULL AUTO_INCREMENT,
  description varchar(1000) NOT NULL,
  enabled BOOLEAN ,
  PRIMARY KEY (code_segment)
);

CREATE TABLE subsidiary (
  code_subsidiary int(11) NOT NULL AUTO_INCREMENT,
  description varchar(1000) NOT NULL,
  enabled BOOLEAN ,
  PRIMARY KEY (code_subsidiary)
);

CREATE TABLE person (
  id_person int(11) NOT NULL ,
  first_name varchar(1000) NOT NULL,
  last_name varchar(1000) NOT NULL,
  address varchar(1000),
  birth datetime ,
  PRIMARY KEY (id_person)
);


CREATE TABLE customer (
  foid int(15) ,
  user_location varchar(100),
  user_info varchar(100),
  id_person_customer int(11),
  code_segment_customer int(11),
  PRIMARY KEY (foid)
);

CREATE TABLE vendor (
  foid_vendor int(15) ,
  user_location varchar(100),
  user_info varchar(100),
  id_person_vendor int(11),
  PRIMARY KEY (foid_vendor)

);

CREATE TABLE product (
  id_product int(15) ,
  desc_product varchar(100),
  origin_product varchar(100),
  PRIMARY KEY (id_product)

);


