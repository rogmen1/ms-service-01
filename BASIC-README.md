
#Micro service for labs for intellingJ

###For local run over H2 Database
``
mvn spring-boot:run -Dapp.profiles=dev
``
###For Unit test just call
``
mvn test -Punit
``
###For Integration test (cucumber) just call
``
mvn integration-test -Pintegration
``

# Pasos para trabajarlo sobre un Entorno GKE 


1.-  se debe crear el cluster, de preferencia auto-piloto
2.-  asegurar que la instancia de artifactory esta activa para poder realizar una compilacion exitosa
3.-  chequear el la seccion Cloud Build -> Settings -> Service account permissions y contar con habilitacion de 
    -Cloud Run	Cloud Run Admin
    -Kubernetes Engine	Kubernetes Engine Developer
    -Service Accounts	Service Account User
    -Compute Engine	Compute Instance Admin (v1)

###For deploy Testing by Cloud Build
``
see task : step-publish-app
``





